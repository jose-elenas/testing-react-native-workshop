const Auth = (
  username: string,
  password: string
): Promise<{ auth: boolean; message: string }> => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let data = username === "jose@elenas.la" && password === "elenas.la";
      resolve({
        auth: data,
        message: data ? "Log In Successfull" : "Log In Failed",
      });
    }, 1000);
  });
};

export { Auth };
