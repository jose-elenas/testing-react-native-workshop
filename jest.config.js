const expoPreset = require("jest-expo/jest-preset.js");

module.exports = {
  preset: "react-native",
  setupFilesAfterEnv: ["@testing-library/jest-native/extend-expect"],
  transformIgnorePatterns: [
    "node_modules/(?!(jest-)?react-native|react-clone-referenced-element|@react-native-community|expo(nent)?|@expo(nent)?/.*|react-navigation|@react-navigation/.*|@unimodules/.*|unimodules|sentry-expo|native-base|@sentry/.*)",
  ],
  testPathIgnorePatterns: ["<rootDir>/node_modules"],
  setupFiles: [...expoPreset.setupFiles],
  coverageReporters: ["text", "cobertura"],
};
