import "react-native";
import React from "react";
import App from "../AppMain";
import { render, fireEvent, waitFor } from "react-native-testing-library";

describe("App", () => {
  jest.setTimeout(30000);

  it("render correctly text dummy", () => {
    const { getByTestId } = render(<App />);
    const textDummy = getByTestId("dummy");
    expect(textDummy.props.children).toMatch(/Open Modal!/);
  });

  it("render and open modal", async () => {
    const { getByTestId, getByPlaceholder, getByText, toJSON } = render(
      <App />
    );
    await waitFor(() => fireEvent.press(getByText("Open")));
    const modal = getByTestId("ModalContainer");
    expect(modal.props.visible).toBe(true);
  });

  it("render and submit Successfull", async () => {
    const { getByTestId, getByPlaceholder, getByText, queryByTestId } = render(
      <App />
    );
    await waitFor(() => fireEvent.press(getByText("Open")));
    fireEvent.changeText(getByPlaceholder("Username"), "jose@elenas.la");
    fireEvent.changeText(getByPlaceholder("Password"), "elenas.la");
    await waitFor(() => fireEvent.press(getByText("Submit")));
    expect(queryByTestId("ModalContainer")).toBeNull();
    expect(getByTestId("message").props.children).toMatch(/Log In Successfull/);
  });

  it("render and submit Successfull", async () => {
    const { getByTestId, getByText, queryByTestId } = render(<App />);
    await waitFor(() => fireEvent.press(getByText("Open")));
    const modalContainer = getByTestId("ModalContainer");
    await waitFor(() => {
      jest.useFakeTimers();
      fireEvent(modalContainer, "onRequestClose");
      jest.runAllTimers();
    });
    expect(queryByTestId("ModalContainer")).toBeNull();
  });
});
