import { Auth } from "../Auth";

describe("Auth", () => {
  it("Auth success", async () => {
    const { auth, message } = await Auth("jose@elenas.la", "elenas.la");
    expect(auth).toBe(true);
  });

  it("Auth failed", async () => {
    const { auth, message } = await Auth("jose@elenas.la", "123454");
    expect(auth).toBe(false);
  });
});
