import "react-native";
import React from "react";
import { Modal } from "../Modal";
import { render, fireEvent, cleanup } from "react-native-testing-library";

describe("Modal forms", () => {
  // afterEach(cleanup);
  jest.useFakeTimers();
  it("Modal success onRequestClose", () => {
    const close = jest.fn();
    const onSubmit = jest.fn();
    const { getByTestId } = render(
      <Modal visible={true} close={close} onSubmit={onSubmit} />
    );
    const modalContainer = getByTestId("ModalContainer");
    fireEvent(modalContainer, "onRequestClose");
    jest.runAllTimers();
    expect(close).toHaveBeenCalledTimes(1);
  });

  it("Modal success touch out side", () => {
    const close = jest.fn();
    const onSubmit = jest.fn();
    const { getByTestId } = render(
      <Modal visible={true} close={close} onSubmit={onSubmit} />
    );
    const touchModalContainer = getByTestId("TouchModalContainer");
    fireEvent(touchModalContainer, "onPressOut");
    jest.runAllTimers();
    expect(close).toHaveBeenCalledTimes(1);
  });

  it("Modal onsubmit called success with params", () => {
    const close = jest.fn();
    const onSubmit = jest.fn();
    const { getByText, getByPlaceholder } = render(
      <Modal visible={true} close={close} onSubmit={onSubmit} />
    );
    fireEvent.changeText(getByPlaceholder("Username"), "jose@elenas.la");
    fireEvent.changeText(getByPlaceholder("Password"), "elenas.la");
    fireEvent.press(getByText("Submit"));
    expect(onSubmit).toBeCalledWith("jose@elenas.la", "elenas.la");
  });
});
