import React, { useState } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Modal } from "./Modal";
import { Auth } from "./Auth";

export default function App() {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [mssg, setMssg] = useState<string>("");

  const onSubmit = async (username: string, password: string) => {
    const { auth, message } = await Auth(username, password);
    setMssg(message);
    handleModal(false);
  };

  const handleModal = (val: boolean) => setShowModal(val);

  return (
    <View style={styles.container}>
      <Text style={{ marginBottom: 10 }} testID="dummy">
        Open Modal!
      </Text>
      <Text style={{ color: "red", marginBottom: 10 }} testID="message">
        {mssg}
      </Text>
      <Button
        onPress={() => handleModal(true)}
        title="Open"
        color="#841584"
        accessibilityLabel="Learn more about this button"
        testID="buttonModal"
      />
      {showModal && (
        <Modal
          close={() => handleModal(false)}
          onSubmit={onSubmit}
          visible={showModal}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
