import React, { FunctionComponent, useRef, useEffect, useState } from "react";
import {
  Modal as ModalView,
  View,
  Animated,
  Dimensions,
  PanResponder,
  TouchableWithoutFeedback,
  StyleSheet,
  TextInput,
  Button,
} from "react-native";

type ModalProps = {
  visible: boolean;
  close: () => void;
  onSubmit: (username: string, password: string) => void;
};

const Modal: FunctionComponent<ModalProps> = ({ visible, close, onSubmit }) => {
  // States
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const panY = useRef(new Animated.Value(Dimensions.get("screen").height))
    .current;

  const _resetPositionAnim = Animated.timing(panY, {
    toValue: 0,
    duration: 300,
  });

  const _closeAnim = Animated.timing(panY, {
    toValue: Dimensions.get("screen").height,
    duration: 500,
  });

  const top = panY.interpolate({
    inputRange: [-1, 0, 1],
    outputRange: [0, 0, 1],
  });

  const _handleDismiss = () => {
    _closeAnim.start(() => {
      close();
    });
  };

  const _panResponders = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => false,
      onPanResponderMove: Animated.event([null, { dy: panY }]),
      onPanResponderRelease: (e, gs) => {
        if (gs.dy > 0 && gs.vy > 1) {
          return _handleDismiss();
        }
        return _resetPositionAnim.start();
      },
    })
  ).current;

  useEffect(() => {
    if (visible) _resetPositionAnim.start();
  }, [visible]);

  return (
    <ModalView
      animated
      animationType="fade"
      visible={visible}
      transparent
      onRequestClose={_handleDismiss}
      testID="ModalContainer"
    >
      <TouchableWithoutFeedback
        onPressOut={_handleDismiss}
        testID="TouchModalContainer"
      >
        <View style={styles.overlay}>
          <Animated.View
            style={[styles.container, { top }]}
            {..._panResponders.panHandlers}
            testID="TouchAnimatedModalContainer"
          >
            {/* MarkUp */}
            <TextInput
              style={styles.inputs}
              onChangeText={(text) => setUsername(text)}
              value={username}
              placeholder="Username"
            />
            <TextInput
              style={styles.inputs}
              onChangeText={(text) => setPassword(text)}
              value={password}
              placeholder="Password"
            />
            <Button
              onPress={() => onSubmit(username, password)}
              title="Submit"
              color="#841584"
            />
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
    </ModalView>
  );
};

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: "rgba(91, 42, 208, 0.9)",
    flex: 1,
    justifyContent: "center",
  },
  container: {
    backgroundColor: "white",
    paddingHorizontal: 32,
    paddingTop: 40,
    paddingBottom: 20,
    borderRadius: 30,
    flex: 0.5,
    alignItems: "center",
    justifyContent: "center",
  },
  inputs: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    width: "80%",
    marginBottom: 15,
  },
  button: {},
});

export { Modal };
